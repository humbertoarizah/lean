const mongoose = require('mongoose');

const connectToDb = () => {
  mongoose.connect(
    'YOUR MONGO URI',
    {
    },
    () => {
      // eslint-disable-next-line no-console
      console.log('connected to database');
    },
  );
};
module.exports = { connectToDb };
