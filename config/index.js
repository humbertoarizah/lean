/* eslint-disable no-console */
const { connectToDb } = require('./db');

const config = () => {
  console.log('Starting Config');

  console.log('Connecting to db');
  connectToDb();
};

module.exports = config;
