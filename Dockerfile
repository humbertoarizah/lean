FROM node:14

# Create app directory
WORKDIR /app

# Install app dependencies
COPY package.json package-lock.json ./
RUN npm i

# Bundle app source
COPY . .

ENV NODE_ENV production


EXPOSE 3000

CMD [ "npm", "start" ]
