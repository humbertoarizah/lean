const { createPurchase } = require('../controllers/purchase.controller');

const purchasesRoutes = (app, route) => {
  app.post(`${route}/`, createPurchase);
};
module.exports = purchasesRoutes;
