const { createSale } = require('../controllers/sale.controller');

const salesRoutes = (app, route) => {
  app.post(`${route}/`, createSale);
};
module.exports = salesRoutes;
