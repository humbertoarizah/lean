const Sale = require("../models/Sale")
const Purchase = require("../models/Purchase")

const createSale = async (req, res) => {
  try {
    const { fecha, cantidad, idProducto, nombreProducto } = req.body
    const realInventory = await Purchase.find({ idProducto }).sort("fecha")
    const inventory = realInventory.filter((x) => x.cantidad > x.cantidadVendida)
    const totalInventory = realInventory.map((x) => x.cantidad).reduce((prev, next) => prev + next)
    const update = []
    if (inventory && inventory.length > 0 && cantidad <= totalInventory) {
      let searchQuantity = cantidad
      for (let i = 0; i < inventory.length; i += 1) {
        const currentInventory = inventory[i]
        const currentQuantity = currentInventory.cantidad - currentInventory.cantidadVendida
        // cantidad buscada menor que cantidad actual, sino solo actualiza con el inventario de current
        if (searchQuantity > currentQuantity) {
          const available = currentInventory.cantidad - currentInventory.cantidadVendida
          const updateQuantity = searchQuantity - available
          if (updateQuantity > 0) {
            update.push([{ _id: currentInventory._id }, { cantidadVendida: currentInventory.cantidad }])
            searchQuantity -= available
          } else {
            const updateValue = currentInventory.cantidad - currentInventory.cantidadVendida - searchQuantity
            update.push([{ _id: currentInventory._id }, { cantidadVendida: updateValue }])
            break
          }
        } else {
          // actualiza inventario de current
          await Purchase.findOneAndUpdate({ _id: currentInventory._id }, { cantidadVendida: currentInventory.cantidadVendida + searchQuantity })
          break
        }
      }
      for (let i = 0; i < update.length; i += 1) {
        const currentUpdate = update[i]
        await Purchase.findOneAndUpdate(update[i][0], update[i][1])
      }

      const sale = await new Sale({ fecha, cantidad, idProducto, nombreProducto }).save()
      res.status(200).json({ error: false, success: true, data: sale })
    } else res.status(508).json({ error: true, success: false, data: { message: "No hay inventario disponible. Por favor comuníquese con un administrador" } })
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
    res.status(500).json({ error: true, success: false, data: err })
  }
}

module.exports = { createSale }
