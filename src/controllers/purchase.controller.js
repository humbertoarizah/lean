const Purchase = require('../models/Purchase')

const createPurchase = async (req, res) => {
  try {
    const {
      fecha, cantidad, idProducto, nombreProducto,
    } = req.body;
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth()
    const count = await Purchase.count({
      fecha: { $gte: new Date(year, month, 1), $lt: new Date(year, month + 1, 0) },
    })
    console.log(count)
    if (count < 30) {
      await new Purchase({
        fecha, cantidad, idProducto, nombreProducto,
      }).save()
      res.status(200).json({ error: false, success: true })
    } else {
      res.status(508).json({ error: true, success: false, data: { message: 'El limite de compras por mes es 30. Por favor contacta un administrador' } })
    }
  } catch (err) {
    console.error(err)
    res.status(500).json({ error: true, success: false, data: err })
  }
};

module.exports = { createPurchase };
