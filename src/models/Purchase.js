const mongoose = require('mongoose');

const PurchaseModel = new mongoose.Schema({
  fecha: { type: Date, default: new Date() },
  cantidad: { type: Number },
  idProducto: { type: String },
  nombreProducto: { type: String },
  cantidadVendida: { type: Number, default: 0 },
});

const Purchase = mongoose.model('Purchase', PurchaseModel);
module.exports = Purchase;
