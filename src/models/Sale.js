const mongoose = require('mongoose');

const SaleModel = new mongoose.Schema({
  fecha: { type: Date, default: new Date() },
  cantidad: { type: Number },
  idProducto: { type: String },
  nombreProducto: { type: String },
});

const Sale = mongoose.model('Sale', SaleModel);
module.exports = Sale;
