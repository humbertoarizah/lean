const express = require('express');
// config
require('./config')();
// routes
const purchasesRoutes = require('./src/routes/purchase.route');
const salesRoutes = require('./src/routes/sales.route');

const app = express();
const port = process.env.PORT || 3000;

// middlewares
app.use(express.json({}));

// Routes
purchasesRoutes(app, '/purchases/');
salesRoutes(app, '/sales/');

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
